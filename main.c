#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <wctype.h>
#include <wchar.h>
#include <stdbool.h>
#include <time.h>

// -- Sekcja stałych --

// - Stałe określające ścieżki (nazwy) plików
// Ścieżka do utworu
#define POEM_FILE "../pan-tadeusz.txt"

// Ścieżka do wyrazów stoplisty
#define STOPWORDS_FILE "../stopwords.txt"

// Ściezka do wygenerowania błędnych wyrazów
#define OUTPUT_FILE "../corrections.txt"

// - Stałe określające ilość przeznaczonej pamięci
// Rozmiar użytej pamięci dla jednego słowa
#define WORD_SIZE 128

// Rozmiar użytej pamięci dla listy wyrazów
#define WORD_LIST_SIZE 65536

// - Stałe określające parametry algorytmu Smith-Waterman
// Ilość punktów za poprawność
#define MATCH_POINTS 3

// Ilość punktów za niepoprawność
#define MISMATCH_POINTS -3

// Ilość punktów za przerwę
#define GAP_POINTS -5


// -- Sekcja struktury --

// Definicja struktury
// Ma to zapewnić możliwość przechowywania w jednym miejscu wyrazów oraz ich ilości
// W C/C++ nie ma możliwości ustalenia ilości elementów w tablicy (takim pojemniku)
// Dlatego konieczne jest przechowywanie ilości słów, aby potem można było sprawnie poruszać się po tablicy
struct words_list {
    wchar_t** words;
    int amount;
};

// Definiowanie typu
// W dalszym kodzie używa się odwołania do typu. Z kolei typ jest odwołaniem do struktury.
typedef struct words_list WordsList;

// -- Sekcja funkcji --

/*
 * Funkcja createWord zwraca nowe słowo.
 * Rozmiar słowa jest oparty na stałej WORD_SIZE
 */
wchar_t* createWord() {
    return calloc(WORD_SIZE, sizeof(wchar_t)); // Rezerwujemy wyczyszczoną pamięć
}

/*
 * Funkcja createWordsList zwraca nową listę słów.
 * Rozmiar listy jest oparty na stałej WORD_LIST_SIZE oraz WORD_SIZE.
 *
 * Słowo to ciąg znaków (np. "kasa" to odpowiednio 4 znaki: "k", "a", "s", "a")
 * Lista rezerwuje pamięć dla tyle słów ile jest w WORD_LIST_SIZE
 * Z kolei na każde słowo rezerwuje na tyle znaków ile określone jest w WORD_SIZE
 */
WordsList createWordsList() {
    return (WordsList) {
        .words = malloc(sizeof(wchar_t) * WORD_SIZE * WORD_LIST_SIZE), // Tutaj rezerwujemy pamięć
        .amount = 0
    };
}

/*
 * Funkcja addWordToList dodaje słowo do listy słów.
 * Jako parametry przekazujemy kolejno listę słów (czyli wordsList) oraz dodawane słowo (czyli word)
 */
void addWordToList(WordsList* wordsList, wchar_t* word) {
    wordsList->words[wordsList->amount++] = word; // Dodajemy słowo do tablicy oraz zwiększamy licznik słów o 1
}

/*
 * Funkcja readWords odczytuje słowa z podanego pliku.
 * Jako parametry przekazujemy odpowiednio nazwę pliku (czyli filename) oraz czy słowa mają być unikalne (czyli unique)
 * Funkcja zwraca listę odczytanych słów.
 *
 * Jeżeli wyrazy mają być unikalne to funkcja wyklucza powtórzenia i lista zawiera niepowtarzalne słowa (czyli nie ma dwóch takich słów w liście)
 *
 * Funkcja odczytuje plik znak-po-znaku. Oznacza to, że program analizuje z kolejna każdą literę pliku.
 */
WordsList readWords(char* filename, const bool unique) {
    FILE *fp; // Deklaracja zmiennej referencji do "uchwytu" pliku
    fp = fopen(filename, "r+,ccs=UTF-8"); // Otwarcie do odczytu pliku podanego w parametrze
    if (fp == NULL) { // Sprawdzenie czy otwarto pomyślnie plik. Jeżeli nie to wyświetla błąd i kończy program.
        wprintf(L"Błąd załadowania pliku.");
        exit(-1);
    }

    // Definiujemy zmienne
    int wordLength = 0; // Początkowo określamy długość wyrazu na 0
    WordsList wordsList = createWordsList(); // Tworzymy listę wyrazów
    wchar_t* word = createWord(); // Tworzymy słowo do zapisania
    wchar_t character; // Deklarujemy zmienną dla znaku

    // Pętla odczytująca kolejno znaki aż do końca pliku. WEOF - koniec pliku
    while ((character = fgetwc(fp)) != WEOF) {
        if (iswalpha(character) && !iswspace(character)) { // Sprawdzamy czy znak jest literą, jeżeli tak:
            word[wordLength++] = towlower(character); // Jeżeli jest literą to dodajemy do słowa
        } else if (wordLength != 0) { // Jeżeli znak nie jest literą to rozpoczynamy procedurę zapisu słowa, czyli:
            bool condition = true; // Deklarujemy zmienną typu logicznego
            if (unique) { // Jeżeli odczytane słowa mają być unikatowe to:
                for (int i = 0; i < wordsList.amount; i++) { // Sprawdzamy każde dotychczas zapisane słowo.
                    if (wcscmp(wordsList.words[i], word) == 0) { // Jeżeli takie słowo istnieje już to:
                        condition = false; // Wykluczamy przez zmianę zmiennej logicznej
                    }
                }
            }

            if (condition) { // Jeżeli słowo nie jest wykluczone
                addWordToList(&wordsList, word); // To dodajemy do listy
            }
            wordLength = 0; // Resetujemy parametry słowa, aby program odczytał kolejne
            word = createWord();
        }
    }
    fclose(fp); // Kończymy odczytywanie pliku

    return wordsList; // Zwracamy listę słów
}

/*
 * Funkcja writeWords zapisuje listę słów do pliku.
 * Jako parametry podaje się nazwę pliku (czyli filename) oraz listę słów (czyli wordsList).
 * Funkcja niczego nie zwraca
 */
void writeWords(char* filename, WordsList wordsList) {
    FILE *fp; // Deklaracja zmiennej referencji do "uchwytu" pliku
    fp = fopen(filename, "w+,ccs=UTF-8"); // Otwarcie pliku do zapisu podanego w parametrze
    if (fp == NULL) { // Jeżeli wystapi błąd przy otwarciu pliku to wyświetla błąd i kończy program
        wprintf(L"Błąd zapisu pliku.");
        exit(-1);
    }

    for (int i = 0; i < wordsList.amount; i++) { // Pętla, która zapisuje po kolei słowa z listy
        fwprintf(fp, L"%ls\n", wordsList.words[i]);
    }
    fclose(fp); // Zamyka plik
}

/*
 * Funkcja deleteChar usuwa znak ze słowa. Funkcja jest używana do tworzenia błędów w słowach.
 * Jako parametr podaje się słowo (czyli word) oraz która litera ma być usunięta (czyli position)
 * Zwraca słowo z usuniętym znakiem
 */
wchar_t* deleteChar(wchar_t* word, int position) {
    wchar_t* dst = createWord();
    wcsncpy(dst, word, position);
    wcscpy(dst+position, word+position+1);
    return dst;
}

/*
 * Funkcja insertChar wstawia znak do słowa. Funkcja jest używana do tworzenia błędów w słowach.
 * Jako parametr podaje się słowo (czyli word), w którym miejscu ma być wstawiony znak (czyli position) oraz jaki to ma być znak (czyli character)
 * Zwraca słowo ze wstawionym znakiem
 */
wchar_t* insertChar(wchar_t* word, int position, wchar_t character) {
    wchar_t* dst = createWord();
    wcsncpy(dst, word, position);
    wcscpy(dst+position, &character);
    wcscpy(dst+position+1, word+position);
    return dst;
}

/*
 * Funkcja replaceChar zamienia znak w słowie. Funkcja jest używana do tworzenia błędów w słowach.
 * Jako parametr podaje się słowo (czyli word), w którym miejscu ma być zmieniony znak (czyli position) oraz jaki to ma być znak (czyli character)
 * Zwraca słowo ze zmienionym znakiem
 *
 * Przykład:
 * dla słowa "myszka" możemy zmienić znak na miejscu czwartym na "h"
 * otrzymamy słowo "myshka"
 */
wchar_t* replaceChar(wchar_t* word, int position, wchar_t character) {
    return insertChar(deleteChar(word, position), position, character);
}

/*
 * Funkcja swapChars zamienia kolejnością znaki w słowie. Funkcja jest używana do tworzenia błędów w słowach.
 * Jako parametr podaje się słowo (czyli word), oraz które znaki mają być zmienione.
 * Zwraca słowo ze zmienionymi znakami
 *
 * Przykład:
 * dla słowa "myszka" możemy zamienić znaki na pozycjach 3 i 4
 * otrzymamy słowo "myhska"
 */
wchar_t* swapChars(wchar_t* word, int position1, int position2) {
    wchar_t character1 = word[position1];
    wchar_t character2 = word[position2];
    return replaceChar(replaceChar(word, position1, character2), position2, character1);
}

/*
 * Funkcja randomChar zwraca losowy znak. Funkcja jest używana do tworzenia błędów w słowach.
 */
wchar_t randomChar() {
    return btowc('a' + (rand() % 26));
}

/*
 * Funkcja mistakeWord robi błędy w podanym słowie.
 * Jako parametr przyjmuje słowo (czyli word).
 * Zwraca słowo ze zmianami.
 */
wchar_t* mistakeWord(wchar_t* word) {
    srand(time(NULL) + wcslen(word)); // Ustawia parametr losowości

    wchar_t* dst = createWord(); // Tworzy nowe słowo
    wcscpy(dst, word); // A następnie kopiuje słowo z parametru do tego nowego
    const int amountMistakes = 2 + rand() % 2; // Ustala losowo ile błędów ma być w słowie (min 2, max 4)
    for (int i = 0; i < amountMistakes; i++) { // W pętli kolejno popełniane są błędy
        wchar_t* new; // Deklarujemy zmienną na zmienione słowo
        int pos = rand() % wcslen(dst); // Losujemy, która litera słowa będzie pomylona

        switch (rand() % 4) { // Losujemy, jaki rodzaj błędu zostanie dokonany
            case 0: // Usunięcie
                new = deleteChar(dst, pos);
                break;
            case 1: // Wstawienie nowej litery
                new = insertChar(dst, pos, randomChar());
                break;
            case 2: // Zamiana litery na inną
                new = replaceChar(dst, pos, randomChar());
                break;
            case 3: // Zamiana kolejnością liter
                new = swapChars(dst, pos, rand() % wcslen(dst));
                break;
        }
        wcscpy(dst, new); // Kopiujemy zmienione słowo do tego słowa docelowego (z innymi błędami)
        free(new); // Usuwamy z pamięci zbyteczne już słowo
    }
    return dst; // Zwracamy docelowe słowo
}

/*
 * Funkcja processPoem przetwarza utwór literacki.
 * Pobiera wyrazy stoplisty oraz wyrazy z utworu. Następnie od wyrazów z utworu wyklucza wyrazy stoplisty.
 * Dodatkowo losuje oraz dokonuje błędy w słowach zapisanych w pliku corrections.txt
 * Funkcja zwraca już gotową listę wyrazów.
 */
WordsList processPoem() {
    WordsList stopwords = readWords(STOPWORDS_FILE, false); // Pobiera wyrazy stoplisty, są one unikatowe, więc program ich nie będzie filtrować
    WordsList words = readWords(POEM_FILE, true); // Pobiera wyrazy z utworu, one będą filtrowane, aby nie było powtórzeń
    WordsList filteredWords = createWordsList(); // Tworzy listę dla wyrazów bez stoplisty
    WordsList randomizedWords = createWordsList(); // Tworzy listę dla "pomylonych" wyrazów

    srand(time(NULL) + stopwords.amount + words.amount); // Ustawia parametr losowości
    for (int i = 0; i < words.amount; i++) { // Pętla sprawdza każde słowo z utworu
        int condition = true; // Deklaracja zmiennej typu logicznego
        for (int j = 0; j < stopwords.amount; j++) { // Pętla sprawdza pojedyncze słowo z utworu czy jest stoplistą
            if (wcscmp(words.words[i], stopwords.words[j]) == 0) { // Jeżeli jest to:
                condition = false; // Ustawia warunek wykluczenia
                break;
            }
        }
        if (condition) { // Jeżeli słowo nie jest wykluczone to:
            addWordToList(&filteredWords, words.words[i]); // Dodaje słowo do listy
            if (randomizedWords.amount < 50 && rand() % 100 < 1) { // Jeżeli lista wylosowanych słów nie jest pełna oraz słowo jest wylosowane to:
                addWordToList(&randomizedWords, mistakeWord(words.words[i])); // Dodaje słowo do listy wylosowanych słów. W tym słowie są dokonane błędy.
            }
        }
    }
    writeWords(OUTPUT_FILE, randomizedWords); // Zapisuje listę wylosowanych słów do pliku
    return filteredWords; // Zwraca listę wszystkich słów z utworu
}

/*
 * Funkcja compareWords wykonuje algorytm Smith-Waterman.
 * Jako parametr przyjmuje dwa słowa do porównania (czyli word1 oraz word2)
 * Zwraca ilość punktów podobieństwa
 */
int compareWords(wchar_t* word1, wchar_t* word2) {
    // Ustalamy rozmiar macierzy na podstawie długości wyrazów
    const size_t sx = wcslen(word1) + 1;
    const size_t sy = wcslen(word2) + 1;

    // Rezerwujemy pamięć dla macierzy
    unsigned int len = sizeof(int*) * sx + sizeof(int) * sx * sy;
    int** matrix = (int**) malloc(len);
    int* ptr = (int*) (matrix + sx);

    // Deklarujemy oraz zerujemy tablicę dwuwymiarową
    for (int i = 0; i < sx; i++) {
        matrix[i] = (ptr + sy * i);
        for (int j = 0; j < sy; j++) {
            matrix[i][j] = 0;
        }
    }

    // Algorytm Smith-Waterman
    int points = 0; // Deklarujemy ilość punktów. Pierwotnie słowo jest ocenione na 0 punktów.
    for (int i = 1; i < sx; i++) { // Sprawdzamy w pętlach każdy wiersz i kolumnę macierzy
        for (int j = 1; j < sy; j++) {
            int values[3];
            if (word1[i - 1] == word2[j - 1]) { // Jeżeli litery się zgadzają, to przyznaj punkty poprawności
                values[0] = matrix[i-1][j-1] + MATCH_POINTS;
            } else { // Jeżeli litery się nie zgadzają, to przynaj karę punktów niepoprawności
                values[0] = matrix[i-1][j-1] + MISMATCH_POINTS;
            }

            // Dodatkowo określamy punkty za przesunięcie liter
            values[1] = matrix[i-1][j] + GAP_POINTS;
            values[2] = matrix[i][j-1] + GAP_POINTS;

            // Określamy, która z punktacji (punkty za poprawność/niepoprawność czy za przesunięcie) daje maksymalny wynik
            int max = 0;
            for (int k = 0; k < 3; k++) {
                if (values[k] > max) {
                    max = values[k];
                }
            }

            // Maksymalną punktację wpisujemy do macierzy
            matrix[i][j] = max;

            // Jeżeli ta wartość jest największa w macierzy, to zapisujemy do zmiennej przeznaczonej do określenia punktacji całego podobieństwa
            if (max > points) {
                points = max;
            }
        }
    }
    return points; // Zwracamy punkty podobieństwa słów
}

/*
 * Funkcja main jest uruchamiana przez program. Tutaj się rozpoczyna cały program.
 */
int main() {
    setlocale(LC_ALL, ""); // Ustawienie polskich znaków
    wprintf(L"Parametry programu\n");
    wprintf(L"\t%-30ls% d\n", L"Punkty za poprawność:", MATCH_POINTS);
    wprintf(L"\t%-30ls% d\n", L"Punkty za niepoprawność:", MISMATCH_POINTS);
    wprintf(L"\t%-30ls% d\n", L"Punkty za przerwę:", GAP_POINTS);

    WordsList poemWords = processPoem(); // Przetworzenie utworu literackiego. Pobranie wyrazów z utworu.
    WordsList correctionsWords = readWords(OUTPUT_FILE, false); // Odczytanie z pliku błędnych słów

    wprintf(L"\t%-30ls% d\n", L"Ilość załadowanych słów:", poemWords.amount);
    wprintf(L"\nPropozycje poprawnych wyrazów:\n\n");
    for (int i = 0; i < correctionsWords.amount; i++) { // Pętla analizuje każde błędne słowo
        int max = 0;
        wchar_t* bestWord = NULL; // Deklarujemy wskaźnik do najlepszego słowa
        for (int j = 0; j < poemWords.amount; j++) { // Pętla analizuje każde słowo z utworu
            int points = compareWords(correctionsWords.words[i], poemWords.words[j]); // Porównujemy błędne słowo ze słowem z utworu
            if (points > max) { // Jeżeli słowo z utworu oddaje najlepiej te błędne to:
                max = points; // Ustawia ilość punktów podobieństwa
                bestWord = poemWords.words[j]; // Oraz ustawia jakie to jest słowo
            }
        }

        wprintf(L"%ls\n", bestWord); // Wypisuje najbardziej prawidłowe słowo
    }
    return 0; // Kończy program
}
